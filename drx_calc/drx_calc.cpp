#include "stdafx.h"
#include "drx_calc.h"

enum DR7_BITS {
    DR7_MODE = 0,
    DR7_TYPE = 16,
    DR7_SIZE = 18,
    DR7_PREDEF = 10
};

enum DR7_MODE {
    DR7_MODE_LOCAL = 1,
    DR7_MODE_GLOBAL = 2
};

enum DR7_TYPE {
    DR7_TYPE_EXEC = 0,
    DR7_TYPE_WRITE = 1,
    DR7_TYPE_RW = 3
};

enum DR7_SIZE {
    DR7_SIZE_1 = 0,
    DR7_SIZE_2 = 1,
    DR7_SIZE_4 = 3
};

enum DR7_DRX {
    DR7_DR0 = 0,
    DR7_DR1 = 1,
    DR7_DR2 = 2,
    DR7_DR3 = 3
};

//variables
static HINSTANCE hInst;
static DWORD dr7_flags = 0;
static DWORD dr6_flags = 0;
static DWORD dr7_flags_multi = 0;
static DWORD dr7_flags_decode = 0;
static int drx_mode = DR7_MODE_LOCAL;
static int drx_type = DR7_TYPE_EXEC;
static int drx_size = DR7_SIZE_1;
static int drx_num = DR7_DR0;
static TCHAR dr7Value[8] = {};
static TCHAR dr7ValueMulti[8] = {};
static TCHAR dr7ValueDecode[8] = {};
static TCHAR dr6Value[8] = {};


//functions
static INT_PTR CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void CalcFlags();

inline void SetBits(unsigned long& dw, DR7_BITS dr7_bits, int bits, int newValue)
{
    int mask = (1 << bits) - 1; // e.g. 1 becomes 0001, 2 becomes 0011, 3 becomes 0111

    int shift = (int)dr7_bits;
    switch (dr7_bits) {
    case DR7_SIZE:
    case DR7_TYPE:
    {
        shift += (drx_num*4);
        break;
    }
    case DR7_MODE:
        shift = drx_num*2;
        break;
    }

    dw = (dw & ~(mask << shift)) | (newValue << shift);
}

void UpdateDR7RadioStates(HWND hWnd)
{
    //mode
    if(BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_MODELOCAL), BM_GETCHECK, 0, 0)) {
        drx_mode = DR7_MODE_LOCAL;
    } else drx_mode = DR7_MODE_GLOBAL;

    //type
    if(BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_TYPEEXEC), BM_GETCHECK, 0, 0)) {
        drx_type = DR7_TYPE_EXEC;
    }
    else if (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_TYPEWRITE), BM_GETCHECK, 0, 0)) {
        drx_type = DR7_TYPE_WRITE;
    } else drx_type = DR7_TYPE_RW;

    //size
    if(BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_SIZE1), BM_GETCHECK, 0, 0)) {
        drx_size = DR7_SIZE_1;
    }
    else if (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_SIZE2), BM_GETCHECK, 0, 0)) {
        drx_size = DR7_SIZE_2;
    } else drx_size = DR7_SIZE_4;

    //drx
    if(BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_DR0), BM_GETCHECK, 0, 0)) {
        drx_num = DR7_DR0;
    }
    else if (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_DR1), BM_GETCHECK, 0, 0)) {
        drx_num = DR7_DR1;
    }
    else if (BST_CHECKED == SendMessage(GetDlgItem(hWnd, IDC_DR2), BM_GETCHECK, 0, 0)) {
        drx_num = DR7_DR2;
    } else drx_num = DR7_DR3;

    CalcFlags();
    SetDlgItemText(hWnd, IDC_DR7VALUE, dr7Value);
}

void CalcFlags() {
    dr7_flags = 0;
    SetBits(dr7_flags, DR7_MODE, 1, drx_mode);
    SetBits(dr7_flags, DR7_TYPE, 2, drx_type);
    SetBits(dr7_flags, DR7_SIZE, 2, drx_size);
    SetBits(dr7_flags, DR7_PREDEF, 1, 1); //predefined
    wsprintf(dr7Value, _T("%08X"), dr7_flags);
}

void UpdateDR6RadioStates(HWND hWnd)
{
    //which DRx got hit ?
    SendMessage(GetDlgItem(hWnd, IDC_DR6DR0), BM_SETCHECK, BST_UNCHECKED, 0);
    SendMessage(GetDlgItem(hWnd, IDC_DR6DR1), BM_SETCHECK, BST_UNCHECKED, 0);
    SendMessage(GetDlgItem(hWnd, IDC_DR6DR2), BM_SETCHECK, BST_UNCHECKED, 0);
    SendMessage(GetDlgItem(hWnd, IDC_DR6DR3), BM_SETCHECK, BST_UNCHECKED, 0);
    if(dr6_flags & 0x1) SendMessage(GetDlgItem(hWnd, IDC_DR6DR0), BM_SETCHECK, BST_CHECKED, 0);
    if(dr6_flags & 0x2) SendMessage(GetDlgItem(hWnd, IDC_DR6DR1), BM_SETCHECK, BST_CHECKED, 0);
    if(dr6_flags & 0x4) SendMessage(GetDlgItem(hWnd, IDC_DR6DR2), BM_SETCHECK, BST_CHECKED, 0);
    if(dr6_flags & 0x8) SendMessage(GetDlgItem(hWnd, IDC_DR6DR3), BM_SETCHECK, BST_CHECKED, 0);

    //was single step?
    SendMessage(GetDlgItem(hWnd, IDC_DR6SINGLESTEP), BM_SETCHECK, BST_UNCHECKED, 0);
    if(dr6_flags & 0x4000) SendMessage(GetDlgItem(hWnd, IDC_DR6SINGLESTEP), BM_SETCHECK, BST_CHECKED, 0);

    //was exception accessing drx ?
    SendMessage(GetDlgItem(hWnd, IDC_DR6EXCEPT), BM_SETCHECK, BST_UNCHECKED, 0);
    if(dr6_flags & 0x2000) SendMessage(GetDlgItem(hWnd, IDC_DR6EXCEPT), BM_SETCHECK, BST_CHECKED, 0);

    //was T-Bit set ?
    SendMessage(GetDlgItem(hWnd, IDC_DR6TBIT), BM_SETCHECK, BST_UNCHECKED, 0);
    if(dr6_flags & 0x8000) SendMessage(GetDlgItem(hWnd, IDC_DR6TBIT), BM_SETCHECK, BST_CHECKED, 0);
}

void ClearDR7DecodedStates(HWND hWnd)
{
    SendMessage(GetDlgItem(hWnd, IDC_DR0MODE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR0TYPE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR0SIZE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR1MODE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR1TYPE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR1SIZE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR2MODE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR2TYPE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR2SIZE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR3MODE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR3TYPE), WM_SETTEXT, NULL, (LPARAM)L"");
    SendMessage(GetDlgItem(hWnd, IDC_DR3SIZE), WM_SETTEXT, NULL, (LPARAM)L"");
}

void UpdateDR7DecodedStates(HWND hWnd)
{
    ClearDR7DecodedStates(hWnd);

    //dr0
    if(dr7_flags_decode & 0x1) SendMessage(GetDlgItem(hWnd, IDC_DR0MODE), WM_SETTEXT, NULL, (LPARAM)L"Local");
    else if(dr7_flags_decode & 0x2) SendMessage(GetDlgItem(hWnd, IDC_DR0MODE), WM_SETTEXT, NULL, (LPARAM)L"Global");

    if(dr7_flags_decode & 0x10000 && dr7_flags_decode & 0x20000) SendMessage(GetDlgItem(hWnd, IDC_DR0TYPE), WM_SETTEXT, NULL, (LPARAM)L"R/W");
    else if(dr7_flags_decode & 0x10000) SendMessage(GetDlgItem(hWnd, IDC_DR0TYPE), WM_SETTEXT, NULL, (LPARAM)L"Write");
    else if (dr7_flags_decode & 0x1 || dr7_flags_decode & 0x2) SendMessage(GetDlgItem(hWnd, IDC_DR0TYPE), WM_SETTEXT, NULL, (LPARAM)L"Execute");

    if(dr7_flags_decode & 0x40000 && dr7_flags_decode & 0x80000) SendMessage(GetDlgItem(hWnd, IDC_DR0SIZE), WM_SETTEXT, NULL, (LPARAM)L"4 Byte");
    else if(dr7_flags_decode & 0x40000) SendMessage(GetDlgItem(hWnd, IDC_DR0SIZE), WM_SETTEXT, NULL, (LPARAM)L"2 Byte");
    else if(dr7_flags_decode & 0x1 || dr7_flags_decode & 0x2) SendMessage(GetDlgItem(hWnd, IDC_DR0SIZE), WM_SETTEXT, NULL, (LPARAM)L"1 Byte");

    //dr1
    if(dr7_flags_decode & 0x4) SendMessage(GetDlgItem(hWnd, IDC_DR1MODE), WM_SETTEXT, NULL, (LPARAM)L"Local");
    if(dr7_flags_decode & 0x8) SendMessage(GetDlgItem(hWnd, IDC_DR1MODE), WM_SETTEXT, NULL, (LPARAM)L"Global");

    if(dr7_flags_decode & 0x100000 && dr7_flags_decode & 0x200000) SendMessage(GetDlgItem(hWnd, IDC_DR1TYPE), WM_SETTEXT, NULL, (LPARAM)L"R/W");
    else if(dr7_flags_decode & 0x100000) SendMessage(GetDlgItem(hWnd, IDC_DR1TYPE), WM_SETTEXT, NULL, (LPARAM)L"Write");
    else if (dr7_flags_decode & 0x4 || dr7_flags_decode & 0x8) SendMessage(GetDlgItem(hWnd, IDC_DR1TYPE), WM_SETTEXT, NULL, (LPARAM)L"Execute");

    if(dr7_flags_decode & 0x400000 && dr7_flags_decode & 0x800000) SendMessage(GetDlgItem(hWnd, IDC_DR1SIZE), WM_SETTEXT, NULL, (LPARAM)L"4 Byte");
    else if(dr7_flags_decode & 0x400000) SendMessage(GetDlgItem(hWnd, IDC_DR1SIZE), WM_SETTEXT, NULL, (LPARAM)L"2 Byte");
    else if(dr7_flags_decode & 0x4 || dr7_flags_decode & 0x8) SendMessage(GetDlgItem(hWnd, IDC_DR1SIZE), WM_SETTEXT, NULL, (LPARAM)L"1 Byte");

    //dr2
    if(dr7_flags_decode & 0x10) SendMessage(GetDlgItem(hWnd, IDC_DR2MODE), WM_SETTEXT, NULL, (LPARAM)L"Local");
    if(dr7_flags_decode & 0x20) SendMessage(GetDlgItem(hWnd, IDC_DR2MODE), WM_SETTEXT, NULL, (LPARAM)L"Global");

    if(dr7_flags_decode & 0x1000000 && dr7_flags_decode & 0x2000000) SendMessage(GetDlgItem(hWnd, IDC_DR2TYPE), WM_SETTEXT, NULL, (LPARAM)L"R/W");
    else if(dr7_flags_decode & 0x1000000) SendMessage(GetDlgItem(hWnd, IDC_DR2TYPE), WM_SETTEXT, NULL, (LPARAM)L"Write");
    else if (dr7_flags_decode & 0x10 || dr7_flags_decode & 0x20) SendMessage(GetDlgItem(hWnd, IDC_DR2TYPE), WM_SETTEXT, NULL, (LPARAM)L"Execute");

    if(dr7_flags_decode & 0x4000000 && dr7_flags_decode & 0x8000000) SendMessage(GetDlgItem(hWnd, IDC_DR2SIZE), WM_SETTEXT, NULL, (LPARAM)L"4 Byte");
    else if(dr7_flags_decode & 0x4000000) SendMessage(GetDlgItem(hWnd, IDC_DR2SIZE), WM_SETTEXT, NULL, (LPARAM)L"2 Byte");
    else if(dr7_flags_decode & 0x10 || dr7_flags_decode & 0x20) SendMessage(GetDlgItem(hWnd, IDC_DR2SIZE), WM_SETTEXT, NULL, (LPARAM)L"1 Byte");

    //dr3
    if(dr7_flags_decode & 0x40) SendMessage(GetDlgItem(hWnd, IDC_DR3MODE), WM_SETTEXT, NULL, (LPARAM)L"Local");
    if(dr7_flags_decode & 0x80) SendMessage(GetDlgItem(hWnd, IDC_DR3MODE), WM_SETTEXT, NULL, (LPARAM)L"Global");

    if(dr7_flags_decode & 0x10000000 && dr7_flags_decode & 0x20000000) SendMessage(GetDlgItem(hWnd, IDC_DR3TYPE), WM_SETTEXT, NULL, (LPARAM)L"R/W");
    else if(dr7_flags_decode & 0x10000000) SendMessage(GetDlgItem(hWnd, IDC_DR3TYPE), WM_SETTEXT, NULL, (LPARAM)L"Write");
    else if (dr7_flags_decode & 0x40 || dr7_flags_decode & 0x80) SendMessage(GetDlgItem(hWnd, IDC_DR3TYPE), WM_SETTEXT, NULL, (LPARAM)L"Execute");

    if(dr7_flags_decode & 0x40000000 && dr7_flags_decode & 0x80000000) SendMessage(GetDlgItem(hWnd, IDC_DR3SIZE), WM_SETTEXT, NULL, (LPARAM)L"4 Byte");
    else if(dr7_flags_decode & 0x40000000) SendMessage(GetDlgItem(hWnd, IDC_DR3SIZE), WM_SETTEXT, NULL, (LPARAM)L"2 Byte");
    else if(dr7_flags_decode & 0x40 || dr7_flags_decode & 0x80) SendMessage(GetDlgItem(hWnd, IDC_DR3SIZE), WM_SETTEXT, NULL, (LPARAM)L"1 Byte");
}

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    hInst = hInstance;

    DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAINWINDOW), NULL, &WndProc);
    ExitProcess(NULL);
}

INT_PTR CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_INITDIALOG:
    {
        //init DR7 control radios
        SendMessage(GetDlgItem(hWnd, IDC_MODELOCAL), BM_SETCHECK, BST_CHECKED, 0);
        SendMessage(GetDlgItem(hWnd, IDC_TYPEEXEC), BM_SETCHECK, BST_CHECKED, 0);
        SendMessage(GetDlgItem(hWnd, IDC_SIZE1), BM_SETCHECK, BST_CHECKED, 0);
        SendMessage(GetDlgItem(hWnd, IDC_DR0), BM_SETCHECK, BST_CHECKED, 0);

        //init DR7 decode radios
        ClearDR7DecodedStates(hWnd);

        CalcFlags();
        SetDlgItemText(hWnd, IDC_DR7VALUE, dr7Value);
        break;
    }

    case WM_COMMAND:
    {
        switch (LOWORD(wParam))
        {
        case IDC_MODELOCAL:
        case IDC_MODEGLOBAL:
        case IDC_TYPEEXEC:
        case IDC_TYPEWRITE:
        case IDC_TYPERW:
        case IDC_SIZE1:
        case IDC_SIZE2:
        case IDC_SIZE4:
        case IDC_DR0:
        case IDC_DR1:
        case IDC_DR2:
        case IDC_DR3: {
            UpdateDR7RadioStates(hWnd);
            break;
        }
        case IDC_DR6VALUE: {
            GetDlgItemText(hWnd, IDC_DR6VALUE, dr6Value, 9);
            dr6_flags = wcstoul(dr6Value, NULL, 16);
            dr6_flags &= ~0xFFFF0FF0; //clear predefined bits

            UpdateDR6RadioStates(hWnd);

            break;
        }
        case IDC_BTNKEEP: {
            dr7_flags_multi |= dr7_flags;
            wsprintf(dr7ValueMulti, _T("%08X"), dr7_flags_multi);
            SetDlgItemText(hWnd, IDC_DR7VALUEMULTI, dr7ValueMulti);
            break;
        }
        case IDC_BTNCLEAR: {
            dr7_flags_multi = 0;
            wsprintf(dr7ValueMulti, _T("%08X"), dr7_flags_multi);
            SetDlgItemText(hWnd, IDC_DR7VALUEMULTI, dr7ValueMulti);
            break;
        }
        case IDC_DR7VALUEDECODE: {
            GetDlgItemText(hWnd, IDC_DR7VALUEDECODE, dr7ValueDecode, 9);
            dr7_flags_decode = wcstoul(dr7ValueDecode, NULL, 16);

            UpdateDR7DecodedStates(hWnd);

            break;
        }

        }

    }
    break;

    case WM_CLOSE:
    {
        EndDialog(hWnd, NULL);
    }
    break;

    default:
    {
        return FALSE;
    }
    }
    return 0;
}