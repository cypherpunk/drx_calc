//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by drx_calc.rc
//
#define IDD_MAINWINDOW                  101
#define IDC_MODELOCAL                   1001
#define IDC_MODEGLOBAL                  1002
#define IDC_TYPEEXEC                    1003
#define IDC_TYPEWRITE                   1004
#define IDC_TYPERW                      1005
#define IDC_SIZE1                       1006
#define IDC_SIZE2                       1007
#define IDC_SIZE4                       1008
#define IDC_DR7VALUE                    1012
#define IDC_MODE                        1013
#define IDC_DR0                         1014
#define IDC_DR1                         1015
#define IDC_DR2                         1016
#define IDC_DR3                         1017
#define IDC_DR6VALUE                    1018
#define IDC_DR6DR0                      1019
#define IDC_DR6DR1                      1020
#define IDC_DR6DR2                      1021
#define IDC_DR6DR3                      1022
#define IDC_DR6SINGLESTEP               1023
#define IDC_DR6EXCEPT                   1024
#define IDC_DR6TBIT                     1025
#define IDC_BTNKEEP                     1026
#define IDC_DR7VALUEMULTI               1027
#define IDC_BTNCLEAR                    1028
#define IDC_DR7VALUEDECODE              1029
#define IDC_DR0MODE                     1030
#define IDC_DR0TYPE                     1031
#define IDC_DR0SIZE                     1032
#define IDC_DR1MODE                     1040
#define IDC_DR1TYPE                     1041
#define IDC_DR1SIZE                     1042
#define IDC_DR2MODE                     1043
#define IDC_DR2TYPE                     1044
#define IDC_DR2SIZE                     1045
#define IDC_RADIO10                     1046
#define IDC_DR3MODE                     1046
#define IDC_DR3TYPE                     1047
#define IDC_DR3SIZE                     1048

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
